package it.cnr.isti.hiis.querymongo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.MongoServerException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.client.model.Sorts;
import it.cnr.isti.hiis.newadaptationengine.model.statistics.Action;
import it.cnr.isti.hiis.newadaptationengine.model.statistics.ActionType;
import it.cnr.isti.hiis.newadaptationengine.model.statistics.ContextEntities;
import it.cnr.isti.hiis.newadaptationengine.model.statistics.Entity;
import it.cnr.isti.hiis.newadaptationengine.model.statistics.RuleInfo;
import it.cnr.isti.hiis.rules.AbsoluteUpdateActionType;
import it.cnr.isti.hiis.rules.BlockType;
import it.cnr.isti.hiis.rules.ComplexNaryEventType;
import it.cnr.isti.hiis.rules.ComplexUnaryEventType;
import it.cnr.isti.hiis.rules.ConditionType;
import it.cnr.isti.hiis.rules.EventType;
import it.cnr.isti.hiis.rules.InvokeFunctionParameterType;
import it.cnr.isti.hiis.rules.InvokeFunctionType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bson.Document;

/**
 *
 * @author Marco Manca
 */
public class query {

    public static void main(String[] aregs) {
        String contextName = "cm_trialpetal_marco";

        MongoClient mongoClient = new MongoClient("localhost");
        MongoDatabase database = mongoClient.getDatabase("RuleManager");

        List<RuleInfo> rules = new LinkedList<>();

        try {            
            ArrayList<Document> createdRules = database.getCollection(contextName)
                    .find()
                    .sort(Sorts.descending("Timestamp"))
                    .into(new ArrayList<>());
            int idx = 0;
            Long ruleId;
            for (Document _rule : createdRules) {
                RuleInfo info = new RuleInfo();
                rules.add(info);
                ruleId = _rule.getLong("RuleId");
                info.setRuleId(ruleId.intValue());
                info.setNaturalLanguage(_rule.getString("NaturalLanguage").trim());
                info.setRuleName(_rule.getString("RuleName"));
                /* when has been triggered */
                ArrayList<Document> ruleDocs = database.getCollection("TriggeredRules")
                        .find(Filters.eq("RuleId", ruleId))
                        .sort(Sorts.descending("Timestamp"))
                        .into(new ArrayList<>());
                for (Document ruleDoc : ruleDocs) {                    
                    Date timestamp = ruleDoc.getDate("Timestamp");
                    info.getTriggeredDateList().add(timestamp);
                }

                /* events - condition - action info */
                ObjectMapper mapper = new ObjectMapper();
                ContextEntities ruleInfo = new ContextEntities();
                analyzeRule(_rule, mapper, ruleInfo);

                info.setUser(ruleInfo.getUser());
                info.setEnvironment(ruleInfo.getEnvironment());
                info.setTechnology(ruleInfo.getTechnology());
                info.setSocial(ruleInfo.getSocial());
                info.setAction(ruleInfo.getAction());

            }
        } catch (MongoServerException ex) {
            Logger.getLogger(query.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (RuleInfo r : rules) {
            System.out.println("Rule ID ".concat(String.valueOf(r.getRuleId())).concat(" Rule Name ").concat(r.getRuleName()));
            System.out.println("Triggered ".concat(String.valueOf(r.getTriggeredDateList().size())).concat(" times"));
            System.out.println(r.toString());
            System.out.println("");
        }
        mongoClient.close();
    }

    private static void analyzeRule(Document doc, ObjectMapper objectMapper, ContextEntities toRet) {
        try {
            EventType event = objectMapper.readValue(doc.getString("Event"), EventType.class);
            analyzeEvent(event, toRet);
            ConditionType condition = objectMapper.readValue(doc.getString("Condition"), ConditionType.class);
            analyzeCondition(condition, toRet);
            List<Object> actions = objectMapper.readValue(doc.getString("Action"), List.class);
            analyzeActions(actions, toRet);
        } catch (IOException ex) {
            Logger.getLogger(query.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void analyzeEvent(EventType event, ContextEntities toRet) {
        if (event == null) {
            return;
        }
        if (event.getSimpleEvent() != null && event.getSimpleEvent().getEntityReference() != null) {
            String xPath = event.getSimpleEvent().getEntityReference().getXPath();
            analyzeXPath(xPath, event.getSimpleEvent().getEntityReference().getDimensionId().intValue(), toRet, 0);
        }
        if (event.getComplexNaryCompEvent() != null) {
            analyzeComplexNaryCompEvent(event.getComplexNaryCompEvent(), toRet);
        }
        if (event.getComplexUnaryCompEvent() != null) {
            analyzeComplexUnaryCompEvent(event.getComplexUnaryCompEvent(), toRet);
        }
    }

    private static void analyzeComplexNaryCompEvent(ComplexNaryEventType complexNaryCompEvent, ContextEntities toRet) {
        for (EventType evt : complexNaryCompEvent.getEvent()) {
            analyzeEvent(evt, toRet);
        }
    }

    private static void analyzeComplexUnaryCompEvent(ComplexUnaryEventType complexUnaryCompEvent, ContextEntities toRet) {
        analyzeEvent(complexUnaryCompEvent.getEvent(), toRet);
    }

    private static void analyzeCondition(ConditionType condition, ContextEntities toRet) {
        if (condition == null) {
            return;
        }
        if (condition.getEntityReference() != null) {
            analyzeXPath(condition.getEntityReference().getXPath(), condition.getEntityReference().getDimensionId().intValue(), toRet, 1);
        }
        for (ConditionType cond : condition.getCondition()) {
            analyzeCondition(cond, toRet);
        }
    }

    private static void analyzeXPath(String xPath, int dimension, ContextEntities entities, int type) {
        if (xPath.startsWith("user/")) {
            if (!entities.contains(xPath, dimension, "user", type)) {
                entities.getUser().add(new Entity(xPath, dimension, type));
            }
        } else if (xPath.startsWith("environment/")) {
            if (!entities.contains(xPath, dimension, "environment", type)) {
                entities.getEnvironment().add(new Entity(xPath, dimension, type));
            }
        } else if (xPath.startsWith("technology/")) {
            if (!entities.contains(xPath, dimension, "technology", type)) {
                entities.getTechnology().add(new Entity(xPath, dimension, type));
            }
        } else if (xPath.startsWith("social/")) {
            if (!entities.contains(xPath, dimension, "social", type)) {
                entities.getSocial().add(new Entity(xPath, dimension, type));
            }
        }
    }

    private static void analyzeActions(List<Object> actions, ContextEntities entities) {
        if (actions == null || actions.isEmpty()) {
            return;
        }
        for (Object obj : actions) {
            if (obj.getClass().equals(LinkedHashMap.class)) {
                LinkedHashMap map = (LinkedHashMap) obj;
                if (map.containsKey("action")) {
                    List actionList = (List) map.get("action");
                    for (Object _obj : actionList) {
                        if (_obj.getClass().equals(LinkedHashMap.class)) {
                            HashMap _map = (LinkedHashMap) _obj;
                            if (_map.containsKey("name") && (_map.get("name").equals("Reminders") || _map.get("name").equals("Alarms"))) {
                                ActionType type = ActionType.REMINDER;
                                String target = "";
                                String constant = "";
                                String recipientKey = "reminderRecipient";
                                if (_map.get("name").equals("Alarms")) {
                                    type = ActionType.ALARM;
                                    recipientKey = "alarmRecipient";
                                }
                                List inputParam = (List) _map.get("input");
                                for (Object objInput : inputParam) {
                                    HashMap mapTmp = (HashMap) objInput;
                                    if (mapTmp.containsKey("name")) {
                                        String paramName = mapTmp.get("name").toString();
                                        if (paramName.equals("notificationMode")) {
                                            HashMap value = (HashMap) mapTmp.get("value");
                                            HashMap constantMap = (HashMap) value.get("constant");
                                            target = constantMap.get("value").toString();
                                        } else if (paramName.equals(recipientKey)) {
                                            HashMap value = (HashMap) mapTmp.get("value");
                                            HashMap constantMap = (HashMap) value.get("constant");
                                            constant = constantMap.get("value").toString();
                                        }
                                    }
                                }
                                if (!entities.contains(type, target, constant)) {
                                    entities.getAction().add(new Action(type, target, constant));
                                }
                            } else if (_map.containsKey("name") && _map.get("name").equals("lightScene")) {
                                String constant = "";
                                String target = "";
                                List inputParam = (List) _map.get("input");
                                for (Object objInput : inputParam) {
                                    HashMap mapTmp = (HashMap) objInput;
                                    if (mapTmp.containsKey("name") && mapTmp.get("name").equals("sceneName")) {
                                        HashMap valueTmp = (HashMap) mapTmp.get("value");
                                        HashMap constantMap = (HashMap) valueTmp.get("constant");
                                        constant = constantMap.get("value").toString();
                                    } else if (mapTmp.containsKey("name") && mapTmp.get("name").equals("room")) {
                                        HashMap valueTmp = (HashMap) mapTmp.get("value");
                                        HashMap constantMap = (HashMap) valueTmp.get("constant");
                                        target = cleanActionTarget(constantMap.get("value").toString()).concat(" Lights Scene");
                                    }
                                }
                                if (!entities.contains(ActionType.APPLIANCE, target, constant)) {
                                    entities.getAction().add(new Action(ActionType.APPLIANCE, target, constant));
                                }
                            } else if (_map.containsKey("entityReference") && _map.containsKey("value")) {
                                //appliance
                                HashMap entityReference = (HashMap) _map.get("entityReference");
                                String target = "";
                                String constant = "";
                                String xPath = "";
                                if (entityReference.containsKey("xpath")) {
                                    xPath = (String) entityReference.get("xpath");
                                    if (xPath.startsWith("applianceState") && xPath.contains("/")) {
                                        String[] xPathSplit = xPath.split("/");
                                        if (xPathSplit.length >= 3) {
                                            String attr = "";
                                            if (xPath.contains("@")) {
                                                int idx = xPath.indexOf("@");
                                                attr = " ".concat(xPath.substring(idx + 1));
                                            }
                                            target = cleanActionTarget(xPathSplit[1]).concat(" ").concat(cleanActionTarget(xPathSplit[2]).concat(attr));
                                        }
                                    }
                                }
                                HashMap value = (HashMap) _map.get("value");
                                if (value.containsKey("constant")) {
                                    HashMap _constant = (HashMap) value.get("constant");
                                    if (_constant.containsKey("value")) {
                                        constant = _constant.get("value").toString();
                                    }
                                }
                                if (!entities.contains(ActionType.APPLIANCE, target, constant)) {
                                    entities.getAction().add(new Action(ActionType.APPLIANCE, target, constant));
                                }
                            }
                        }
                    }
                }
            } else if (obj.getClass().equals(BlockType.class)) {
                BlockType actionList = (BlockType) obj;
                for (Object actionObj : actionList.getAction()) {
                    if (actionObj.getClass().equals(AbsoluteUpdateActionType.class)) {
                        AbsoluteUpdateActionType update = (AbsoluteUpdateActionType) actionObj;
                        String target = "";
                        String constant = "";
                        if (update.getEntityReference() != null) {
                            String xPath = update.getEntityReference().getXPath();
                            if (xPath.startsWith("applianceState") && xPath.contains("/")) {
                                String[] xPathSplit = xPath.split("/");
                                if (xPathSplit.length >= 3) {
                                    target = cleanActionTarget(xPathSplit[1]).concat(" ").concat(cleanActionTarget(xPathSplit[2]));
                                }
                            }
                        }
                        if (update.getValue() != null && update.getValue().getConstant() != null) {
                            constant = update.getValue().getConstant().getValue();
                        }
                        if (!entities.contains(ActionType.APPLIANCE, target, constant)) {
                            entities.getAction().add(new Action(ActionType.APPLIANCE, target, constant));
                        }
                    } else if (actionObj.getClass().equals(InvokeFunctionType.class)) {
                        InvokeFunctionType invoke = (InvokeFunctionType) actionObj;
                        ActionType type = ActionType.ALARM;
                        String target = "";
                        String constant = "";
                        if (invoke.getName().equals("Reminders")) {
                            type = ActionType.REMINDER;
                        }
                        for (InvokeFunctionParameterType param : invoke.getInput()) {
                            if (param.getName().equals("Mode")
                                    && param.getValue() != null
                                    && param.getValue().getConstant() != null) {
                                target = param.getValue().getConstant().getValue();
                            } else if ((param.getName().equals("reminderRecipient")
                                    || param.getName().equals("alarmRecipient"))
                                    && param.getValue() != null
                                    && param.getValue().getConstant() != null) {
                                constant = param.getValue().getConstant().getValue();
                            }
                        }
                        if (!entities.contains(type, target, constant)) {
                            entities.getAction().add(new Action(type, target, constant));
                        }
                    }
                }
            }
        }
    }

    private static String cleanActionTarget(String target) {
        switch (target) {
            case "lightColor":
                return "Light Color";
            case "greatLuminare":
                return "Great Luminare";
            case "LivingRoom":
                return "Living Room";
            case "All":
                return "All Rooms";
            case "AllLight":
                return "All Lights";
            default:
                return target;
        }
    }
}
