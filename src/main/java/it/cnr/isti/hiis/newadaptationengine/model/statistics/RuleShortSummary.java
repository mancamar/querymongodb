package it.cnr.isti.hiis.newadaptationengine.model.statistics;

/**
 *
 * @author Marco Manca
 */
public class RuleShortSummary {
    private long numRuleCreated;
    private long numRuleTriggered;
    private long numRuleActive;

    public RuleShortSummary() {
    }

    public RuleShortSummary(long numRuleCreated, long numRuleTriggered, long numRuleActive) {
        this.numRuleCreated = numRuleCreated;
        this.numRuleTriggered = numRuleTriggered;
        this.numRuleActive = numRuleActive;
    }
    
    public long getNumRuleCreated() {
        return numRuleCreated;
    }

    public void setNumRuleCreated(long numRuleCreated) {
        this.numRuleCreated = numRuleCreated;
    }

    public long getNumRuleTriggered() {
        return numRuleTriggered;
    }

    public void setNumRuleTriggered(long numRuleTriggered) {
        this.numRuleTriggered = numRuleTriggered;
    }

    public long getNumRuleActive() {
        return numRuleActive;
    }

    public void setNumRuleActive(long numRuleActive) {
        this.numRuleActive = numRuleActive;
    }        
}
