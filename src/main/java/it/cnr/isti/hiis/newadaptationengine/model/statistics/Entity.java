package it.cnr.isti.hiis.newadaptationengine.model.statistics;

/**
 *
 * @author Marco Manca
 */
public class Entity {
    private String xPath;
    private int type;
    private int dimensionId;
    private int num;

    public Entity() {
        this.num = 1;
    }

    public Entity(String xPath, int dimensionId, int type) {
        this.xPath = xPath;
        this.dimensionId = dimensionId;
        this.type = type;
        this.num = 1;
    }

    public String getxPath() {
        return xPath;
    }

    public void setxPath(String xPath) {
        this.xPath = xPath;
    }

    public int getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(int dimensionId) {
        this.dimensionId = dimensionId;
    }
    
    public void incrementNum() {
        num++;        
    }

    public int getNum() {
        return num;
    }

    public int getType() {
        return type;
    }
    
    
}
