package it.cnr.isti.hiis.newadaptationengine.model.statistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

/**
 *
 * @author Marco Manca
 */
public class TriggeredRules {
    private String ruleId;
    private String ruleName;
    private String context;
    private String naturalLanguage;
    private String userName;
    private String appName;
    private long triggeredTimes;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSSZ")
    private Date creationDate;
    private boolean active;
    
    public TriggeredRules() {
        this.active = false;
    }

    public TriggeredRules(String ruleId, String ruleName, String context, String naturalLanguage, String userName, String appName, long triggeredTimes, Date date, boolean isActive) {
        this.ruleId = ruleId;
        this.ruleName = ruleName;
        this.context = context;
        this.naturalLanguage = naturalLanguage;
        this.userName = userName;
        this.appName = appName;
        this.triggeredTimes = triggeredTimes;
        this.creationDate = date;
        this.active = isActive;
    }

    
    public String getRuleId() {
        return ruleId;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }

    public String getNaturalLanguage() {
        return naturalLanguage;
    }

    public void setNaturalLanguage(String naturalLanguage) {
        this.naturalLanguage = naturalLanguage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public long getTriggeredTimes() {
        return triggeredTimes;
    }

    public void setTriggeredTimes(long triggeredTimes) {
        this.triggeredTimes = triggeredTimes;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    
}
