package it.cnr.isti.hiis.newadaptationengine.model.statistics;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class ContextEntities {
    private List<Entity> user;
    private List<Entity> environment;
    private List<Entity> technology;
    private List<Entity> social;
    private List<Action> action;
    

    public ContextEntities() {        
    }
    
    public List<Entity> getUser() {
        if(user == null)
            user = new LinkedList<>();
        return user;
    }

    public List<Entity> getEnvironment() {
        if(environment == null)
            environment = new LinkedList<>();
        return environment;
    }

    public List<Entity> getTechnology() {
        if(technology == null)
            technology = new LinkedList<>();
        return technology;
    }

    public List<Entity> getSocial() {
        if(social == null)
            social = new LinkedList<>();
        return social;
    }
    
    public boolean contains(String xPath, int dimension, String dimensionName, int type) {
        List<Entity> entList;
        switch(dimensionName) {
            case "user":
                entList = getUser();
                break;
            case "environment":
                entList = getEnvironment();
                break;
            case "technology":
                entList = getTechnology();
                break;
            case "social":
                entList = getSocial();
                break;
            default:
                return false;                
        }
        for(Entity ent : entList) {
            if(ent.getxPath().equals(xPath) && ent.getDimensionId() == dimension &&
                    ent.getType() == type) {
                ent.incrementNum();
                return true;
            } 
        }
        return false;
    }

    public List<Action> getAction() {
        if(this.action == null)
            this.action = new LinkedList<>();
        return action;
    }
    
    public boolean contains(ActionType actionType, String target, String constant) {        
        for(Action action : this.getAction()) {
            if(action.getType().equals(actionType) && action.getTarget().equals(target) &&
                    action.getConstant().equals(constant)) {
                action.incrementNum();
                return true;
            } 
        }
        return false;
    }
}
