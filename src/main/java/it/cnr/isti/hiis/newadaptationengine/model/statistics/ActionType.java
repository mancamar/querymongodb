package it.cnr.isti.hiis.newadaptationengine.model.statistics;

/**
 *
 * @author Marco Manca
 */
public enum ActionType {
    APPLIANCE, REMINDER, ALARM
}