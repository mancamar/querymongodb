package it.cnr.isti.hiis.newadaptationengine.model.statistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class RuleInfo {
    private int ruleId;
    private String ruleName;
    private String naturalLanguage;
    
    private List<Entity> user;
    private List<Entity> environment;
    private List<Entity> technology;
    private List<Entity> social;
    private List<Action> action;
    
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSSZ")
    private List<Date> triggeredDateList;

    public RuleInfo() {
    }

    public RuleInfo(int ruleId, String ruleName, String naturalLanguage) {
        this.ruleId = ruleId;
        this.ruleName = ruleName;
        this.naturalLanguage = naturalLanguage;        
    }

    public int getRuleId() {
        return ruleId;
    }

    public void setRuleId(int ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getNaturalLanguage() {
        return naturalLanguage;
    }

    public void setNaturalLanguage(String naturalLanguage) {
        this.naturalLanguage = naturalLanguage;
    }

    public List<Date> getTriggeredDateList() {
        if(triggeredDateList == null) {
            triggeredDateList = new LinkedList<>();
        }
        return triggeredDateList;
    }

    public void setUser(List<Entity> user) {
        this.user = user;
    }

    public void setEnvironment(List<Entity> environment) {
        this.environment = environment;
    }

    public void setTechnology(List<Entity> technology) {
        this.technology = technology;
    }

    public void setSocial(List<Entity> social) {
        this.social = social;
    }

    public void setAction(List<Action> action) {
        this.action = action;
    }

    public List<Entity> getUser() {
        if(user == null)
            user = new LinkedList<>();
        return user;
    }

    public List<Entity> getEnvironment() {
        if(environment == null)
            environment = new LinkedList<>();
        return environment;
    }

    public List<Entity> getTechnology() {
        if(technology == null)
            technology = new LinkedList<>();
        return technology;
    }

    public List<Entity> getSocial() {
        if(social == null)
            social = new LinkedList<>();
        return social;
    }

    public List<Action> getAction() {
        if(action == null)
            action = new LinkedList<>();
        return action;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();        
        for(Entity e : user) {
            buf.append("User");
            buf.append(" xpath ").append(e.getxPath()).append(" dimension ").append(e.getDimensionId());            
        }        
        for(Entity e : environment) {
            buf.append("Environment");
            buf.append(" xpath ").append(e.getxPath()).append(" dimension ").append(e.getDimensionId());
        }        
        for(Entity e : technology) {
            buf.append("Technology");
            buf.append(" xpath ").append(e.getxPath()).append(" dimension ").append(e.getDimensionId());
        }
        
        return buf.toString();
    }
    
    
    
    
}
