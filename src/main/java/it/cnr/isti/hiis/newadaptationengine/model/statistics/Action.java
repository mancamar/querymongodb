package it.cnr.isti.hiis.newadaptationengine.model.statistics;

/**
 *
 * @author Marco Manca
 */
public class Action {
    private ActionType type;
    //notification, sms, mail, 
    //applianceState/Bedroom/lightColor/@state --> bedroom, kitchen, etc
    private String target;
    //mancamar@tiscali.it, +393403536542
    //On, off, #ff0011, 
    private String constant;
    private int num;

    public Action() {
    }

    public Action(ActionType type, String target, String constant) {
        this.type = type;
        this.target = target;
        this.constant = constant;
        this.num = 1;
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getConstant() {
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }
    
    public void incrementNum() {
        this.num++;        
    }

    public int getNum() {
        return num;
    }
        
}


