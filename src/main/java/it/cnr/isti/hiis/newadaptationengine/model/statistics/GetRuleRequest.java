package it.cnr.isti.hiis.newadaptationengine.model.statistics;

/**
 *
 * @author Marco Manca
 */
public class GetRuleRequest {
    private String contextURL;
    private String userName;
    private String appName;

    public GetRuleRequest() {
    }

    public String getContextURL() {
        return contextURL;
    }

    public void setContextURL(String contextURL) {
        this.contextURL = contextURL;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
    
    
    
}
