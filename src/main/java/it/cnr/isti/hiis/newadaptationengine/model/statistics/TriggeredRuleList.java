package it.cnr.isti.hiis.newadaptationengine.model.statistics;

import java.util.List;

/**
 *
 * @author Marco Manca
 */
public class TriggeredRuleList {
    private List<TriggeredRules> data;

    public TriggeredRuleList() {
    }

    public TriggeredRuleList(List<TriggeredRules> data) {
        this.data = data;
    }

    
    public List<TriggeredRules> getData() {
        return data;
    }

    public void setData(List<TriggeredRules> data) {
        this.data = data;
    }
    
    
}
