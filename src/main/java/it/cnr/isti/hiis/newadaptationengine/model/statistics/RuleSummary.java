package it.cnr.isti.hiis.newadaptationengine.model.statistics;

import it.cnr.isti.hiis.rules.ActionsType;
import it.cnr.isti.hiis.rules.ConditionType;
import it.cnr.isti.hiis.rules.EventType;
import java.util.Date;

/**
 *
 * @author Marco Manca
 */
public class RuleSummary {
    private final int ruleId;
    private final String ruleName;
    private final String naturalLanguage;
    private final String userName;
    private final String appName;
    private final EventType event;
    private final ConditionType condition;
    private final ActionsType action;
    private final Date timestamp;

    public RuleSummary(int ruleId, String ruleName, String naturalLanguage, String userName, String appName, EventType event, ConditionType condition, ActionsType action, Date timestamp) {
        this.ruleId = ruleId;
        this.ruleName = ruleName;
        this.naturalLanguage = naturalLanguage;
        this.userName = userName;
        this.appName = appName;
        this.event = event;
        this.condition = condition;
        this.action = action;
        this.timestamp = timestamp;
    }

    public int getRuleId() {
        return ruleId;
    }

    public String getRuleName() {
        return ruleName;
    }

    public String getNaturalLanguage() {
        return naturalLanguage;
    }

    public String getUserName() {
        return userName;
    }

    public String getAppName() {
        return appName;
    }

    public EventType getEvent() {
        return event;
    }

    public ConditionType getCondition() {
        return condition;
    }

    public ActionsType getAction() {
        return action;
    }

    public Date getTimestamp() {
        return timestamp;
    }
    
    
}
