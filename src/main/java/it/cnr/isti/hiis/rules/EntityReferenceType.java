//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EntityReferenceType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="EntityReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="xPath" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="dimensionId" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="externalModelId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntityReferenceType")
public class EntityReferenceType  implements Cloneable{

    @XmlAttribute(name = "xPath", required = true)
    protected String xPath;
    @XmlAttribute(name = "dimensionId")
    protected Long dimensionId;
    @XmlAttribute(name = "externalModelId")
    protected String externalModelId;

    /**
     * Recupera il valore della propriet� xPath.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXPath() {
        return xPath;
    }

    /**
     * Imposta il valore della propriet� xPath.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXPath(String value) {
        this.xPath = value;
    }

    /**
     * Recupera il valore della propriet� dimensionId.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDimensionId() {
        return dimensionId;
    }

    /**
     * Imposta il valore della propriet� dimensionId.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDimensionId(Long value) {
        this.dimensionId = value;
    }

    /**
     * Recupera il valore della propriet� externalModelId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalModelId() {
        return externalModelId;
    }

    /**
     * Imposta il valore della propriet� externalModelId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalModelId(String value) {
        this.externalModelId = value;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        EntityReferenceType newEntityRef = new EntityReferenceType(); 
        newEntityRef.setExternalModelId(this.getExternalModelId());
        newEntityRef.setXPath(this.getXPath());
        return newEntityRef;
    }
}
