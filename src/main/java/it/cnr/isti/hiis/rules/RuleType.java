//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per RuleType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="RuleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="event" type="{http://urano.isti.cnr.it:8080/cm}EventType" minOccurs="0"/>
 *         &lt;element name="condition" type="{http://urano.isti.cnr.it:8080/cm}ConditionType" minOccurs="0"/>
 *         &lt;element name="action" type="{http://urano.isti.cnr.it:8080/cm}BlockType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="priority" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="originalId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="adaptationEngineEndpoint" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="verified" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleType", propOrder = {
    "event",
    "condition",
    "actionOrRuleOrBlockReference",
    "elseActionOrElseRuleOrElseBlockReference"
})
public class RuleType {

    protected EventType event;
    protected ConditionType condition;
    @JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include= JsonTypeInfo.As.WRAPPER_OBJECT,
        property = "actionOrRuleOrBlockReference"            
    )    
    @JsonSubTypes({
        @JsonSubTypes.Type(value = BlockType.class, name="actionList"),
        @JsonSubTypes.Type(value = RuleType.class, name="rule"),
        @JsonSubTypes.Type(value = NamedEntityReferenceType.class, name="blockReference")
        })    
    @XmlElements({
        @XmlElement(name = "action", type = BlockType.class),
        @XmlElement(name = "rule", type = RuleType.class),
        @XmlElement(name = "blockReference", type = NamedEntityReferenceType.class)
    })        
    protected List<Object> actionOrRuleOrBlockReference;
    @XmlElements({
        @XmlElement(name = "else_action", type = BlockType.class),
        @XmlElement(name = "else_rule", type = RuleType.class),
        @XmlElement(name = "else_blockReference", type = NamedEntityReferenceType.class)
    })
    protected List<Object> elseActionOrElseRuleOrElseBlockReference;
    @XmlAttribute(name = "priority")
    protected Integer priority;
    @XmlAttribute(name = "id")
    protected Long id;
    @XmlAttribute(name = "originalId")
    protected String originalId;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "adaptationEngineEndpoint")
    protected String adaptationEngineEndpoint;
    @XmlAttribute(name = "verified")
    protected Boolean verified;
    @XmlAttribute(name = "Hjid")
    protected Long hjid;
    @XmlAttribute(name = "naturalLanguage")
    protected String naturalLanguage;
    @XmlAttribute(name = "actionMode", required = false)    
    protected String actionMode;
    /**
     * Recupera il valore della propriet� event.
     * 
     * @return
     *     possible object is
     *     {@link EventType }
     *     
     */
    public EventType getEvent() {
        return event;
    }

    /**
     * Imposta il valore della propriet� event.
     * 
     * @param value
     *     allowed object is
     *     {@link EventType }
     *     
     */
    public void setEvent(EventType value) {
        this.event = value;
    }

    /**
     * Recupera il valore della propriet� condition.
     * 
     * @return
     *     possible object is
     *     {@link ConditionType }
     *     
     */
    public ConditionType getCondition() {
        return condition;
    }

    /**
     * Imposta il valore della propriet� condition.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionType }
     *     
     */
    public void setCondition(ConditionType value) {
        this.condition = value;
    }

    /**
     * Recupera il valore della propriet� priority.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * Imposta il valore della propriet� priority.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPriority(Integer value) {
        this.priority = value;
    }

    /**
     * Recupera il valore della propriet� id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Imposta il valore della propriet� id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Recupera il valore della propriet� originalId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalId() {
        return originalId;
    }

    /**
     * Imposta il valore della propriet� originalId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalId(String value) {
        this.originalId = value;
    }

    /**
     * Recupera il valore della propriet� name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il valore della propriet� name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della propriet� adaptationEngineEndpoint.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdaptationEngineEndpoint() {
        return adaptationEngineEndpoint;
    }

    /**
     * Imposta il valore della propriet� adaptationEngineEndpoint.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdaptationEngineEndpoint(String value) {
        this.adaptationEngineEndpoint = value;
    }

    /**
     * Recupera il valore della propriet� verified.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVerified() {
        return verified;
    }

    /**
     * Imposta il valore della propriet� verified.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVerified(Boolean value) {
        this.verified = value;
    }

    public Long getHjid() {
        return hjid;
    }

    public void setHjid(Long hjid) {
        this.hjid = hjid;
    }

    public String getNaturalLanguage() {
        return naturalLanguage;
    }

    public void setNaturalLanguage(String naturalLanguage) {
        this.naturalLanguage = naturalLanguage;
    }

    public String getActionMode() {
        return actionMode;
    }

    public void setActionMode(String actionMode) {
        this.actionMode = actionMode;
    }
    
    /**
     * Gets the value of the actionOrRuleOrBlockReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actionOrRuleOrBlockReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActionOrRuleOrBlockReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BlockType }
     * {@link RuleType }
     * {@link NamedEntityReferenceType }
     * 
     * 
     */
    public List<Object> getActionOrRuleOrBlockReference() {
        if (actionOrRuleOrBlockReference == null) {
            actionOrRuleOrBlockReference = new ArrayList<Object>();
        }
        return this.actionOrRuleOrBlockReference;
    }

    /**
     * Gets the value of the elseActionOrElseRuleOrElseBlockReference property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elseActionOrElseRuleOrElseBlockReference property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElseActionOrElseRuleOrElseBlockReference().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BlockType }
     * {@link RuleType }
     * {@link NamedEntityReferenceType }
     * 
     * 
     */
    public List<Object> getElseActionOrElseRuleOrElseBlockReference() {
        if (elseActionOrElseRuleOrElseBlockReference == null) {
            elseActionOrElseRuleOrElseBlockReference = new ArrayList<Object>();
        }
        return this.elseActionOrElseRuleOrElseBlockReference;
    }

}
