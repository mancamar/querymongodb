//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.07.21 at 04:03:59 PM CEST 
//


package it.cnr.isti.hiis.rules;

import eu.serenoa_fp7.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IfThenElseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IfThenElseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="condition" type="{http://www.serenoa-fp7.eu/}ConditionType"/>
 *         &lt;element name="then" type="{http://www.serenoa-fp7.eu/}BlockType"/>
 *         &lt;element name="elseIf" type="{http://www.serenoa-fp7.eu/}IfThenType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="else" type="{http://www.serenoa-fp7.eu/}BlockType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IfThenElseType", propOrder = {
    "condition",
    "then",
    "elseIf",
    "_else"
})
public class IfThenElseType {

    @XmlElement(required = true)
    protected ConditionType condition;
    @XmlElement(required = true)
    protected BlockType then;
    protected List<IfThenType> elseIf;
    @XmlElement(name = "else")
    protected BlockType _else;

    /**
     * Gets the value of the condition property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionType }
     *     
     */
    public ConditionType getCondition() {
        return condition;
    }

    /**
     * Sets the value of the condition property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionType }
     *     
     */
    public void setCondition(ConditionType value) {
        this.condition = value;
    }

    /**
     * Gets the value of the then property.
     * 
     * @return
     *     possible object is
     *     {@link BlockType }
     *     
     */
    public BlockType getThen() {
        return then;
    }

    /**
     * Sets the value of the then property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockType }
     *     
     */
    public void setThen(BlockType value) {
        this.then = value;
    }

    /**
     * Gets the value of the elseIf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elseIf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElseIf().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IfThenType }
     * 
     * 
     */
    public List<IfThenType> getElseIf() {
        if (elseIf == null) {
            elseIf = new ArrayList<IfThenType>();
        }
        return this.elseIf;
    }

    /**
     * Gets the value of the else property.
     * 
     * @return
     *     possible object is
     *     {@link BlockType }
     *     
     */
    public BlockType getElse() {
        return _else;
    }

    /**
     * Sets the value of the else property.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockType }
     *     
     */
    public void setElse(BlockType value) {
        this._else = value;
    }

}
