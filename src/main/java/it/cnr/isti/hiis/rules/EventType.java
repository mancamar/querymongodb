//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EventType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="EventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="simple_event" type="{http://urano.isti.cnr.it:8080/cm}SimpleEventType"/>
 *         &lt;element name="complex_unary_comp_event" type="{http://urano.isti.cnr.it:8080/cm}ComplexUnaryEventType"/>
 *         &lt;element name="complex_nary_comp_event" type="{http://urano.isti.cnr.it:8080/cm}ComplexNaryEventType"/>
 *       &lt;/choice>
 *       &lt;attribute name="event_name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="event_id" use="required" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventType", propOrder = {
    "simpleEvent",
    "complexUnaryCompEvent",
    "complexNaryCompEvent"
})
public class EventType {

    @XmlElement(name = "simple_event")
    protected SimpleEventType simpleEvent;
    @XmlElement(name = "complex_unary_comp_event")
    protected ComplexUnaryEventType complexUnaryCompEvent;
    @XmlElement(name = "complex_nary_comp_event")
    protected ComplexNaryEventType complexNaryCompEvent;
    @XmlAttribute(name = "event_name")
    protected String eventName;
    @XmlAttribute(name = "event_id", required = true)
    protected String eventId;

    /**
     * Recupera il valore della propriet� simpleEvent.
     * 
     * @return
     *     possible object is
     *     {@link SimpleEventType }
     *     
     */
    public SimpleEventType getSimpleEvent() {
        return simpleEvent;
    }

    /**
     * Imposta il valore della propriet� simpleEvent.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleEventType }
     *     
     */
    public void setSimpleEvent(SimpleEventType value) {
        this.simpleEvent = value;
    }

    /**
     * Recupera il valore della propriet� complexUnaryCompEvent.
     * 
     * @return
     *     possible object is
     *     {@link ComplexUnaryEventType }
     *     
     */
    public ComplexUnaryEventType getComplexUnaryCompEvent() {
        return complexUnaryCompEvent;
    }

    /**
     * Imposta il valore della propriet� complexUnaryCompEvent.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexUnaryEventType }
     *     
     */
    public void setComplexUnaryCompEvent(ComplexUnaryEventType value) {
        this.complexUnaryCompEvent = value;
    }

    /**
     * Recupera il valore della propriet� complexNaryCompEvent.
     * 
     * @return
     *     possible object is
     *     {@link ComplexNaryEventType }
     *     
     */
    public ComplexNaryEventType getComplexNaryCompEvent() {
        return complexNaryCompEvent;
    }

    /**
     * Imposta il valore della propriet� complexNaryCompEvent.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplexNaryEventType }
     *     
     */
    public void setComplexNaryCompEvent(ComplexNaryEventType value) {
        this.complexNaryCompEvent = value;
    }

    /**
     * Recupera il valore della propriet� eventName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Imposta il valore della propriet� eventName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Recupera il valore della propriet� eventId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * Imposta il valore della propriet� eventId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventId(String value) {
        this.eventId = value;
    }

}
