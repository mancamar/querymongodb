//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.19 alle 01:15:36 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per IfThenType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IfThenType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="condition" type="{http://urano.isti.cnr.it:8080/cm}ConditionType"/>
 *         &lt;element name="then" type="{http://urano.isti.cnr.it:8080/cm}BlockType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IfThenType", propOrder = {
    "condition",
    "then"
})
public class IfThenType {

    @XmlElement(required = true)
    protected ConditionType condition;
    @XmlElement(required = true)
    protected BlockType then;

    /**
     * Recupera il valore della propriet� condition.
     * 
     * @return
     *     possible object is
     *     {@link ConditionType }
     *     
     */
    public ConditionType getCondition() {
        return condition;
    }

    /**
     * Imposta il valore della propriet� condition.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionType }
     *     
     */
    public void setCondition(ConditionType value) {
        this.condition = value;
    }

    /**
     * Recupera il valore della propriet� then.
     * 
     * @return
     *     possible object is
     *     {@link BlockType }
     *     
     */
    public BlockType getThen() {
        return then;
    }

    /**
     * Imposta il valore della propriet� then.
     * 
     * @param value
     *     allowed object is
     *     {@link BlockType }
     *     
     */
    public void setThen(BlockType value) {
        this.then = value;
    }

}
