package it.cnr.isti.hiis.rules;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "actionsList")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actionsList", propOrder = {    
    "actions"
})
public class ActionsType {
    @XmlElement(required = true)
    protected List<BlockType> actions;

    public List<BlockType> getActions() {
        if (actions == null) {
            actions = new ArrayList<BlockType>();
        }
        return this.actions;
    }
        
}
