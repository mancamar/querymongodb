//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ConditionOperatorType.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="ConditionOperatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="contains"/>
 *     &lt;enumeration value="starts"/>
 *     &lt;enumeration value="ends"/>
 *     &lt;enumeration value="gt"/>
 *     &lt;enumeration value="lt"/>
 *     &lt;enumeration value="gteq"/>
 *     &lt;enumeration value="lteq"/>
 *     &lt;enumeration value="eq"/>
 *     &lt;enumeration value="neq"/>
 *     &lt;enumeration value="not"/>
 *     &lt;enumeration value="and"/>
 *     &lt;enumeration value="or"/>
 *     &lt;enumeration value="xor"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ConditionOperatorType")
@XmlEnum
public enum ConditionOperatorType {

    @XmlEnumValue("contains")
    CONTAINS("contains"),
    @XmlEnumValue("starts")
    STARTS("starts"),
    @XmlEnumValue("ends")
    ENDS("ends"),
    @XmlEnumValue("gt")
    GT("gt"),
    @XmlEnumValue("lt")
    LT("lt"),
    @XmlEnumValue("gteq")
    GTEQ("gteq"),
    @XmlEnumValue("lteq")
    LTEQ("lteq"),
    @XmlEnumValue("eq")
    EQ("eq"),
    @XmlEnumValue("neq")
    NEQ("neq"),
    @XmlEnumValue("not")
    NOT("not"),
    @XmlEnumValue("and")
    AND("and"),
    @XmlEnumValue("or")
    OR("or"),
    @XmlEnumValue("xor")
    XOR("xor");
    private final String value;

    ConditionOperatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ConditionOperatorType fromValue(String v) {
        for (ConditionOperatorType c: ConditionOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
