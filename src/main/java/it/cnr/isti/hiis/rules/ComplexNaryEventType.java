//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ComplexNaryEventType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ComplexNaryEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="event" type="{http://urano.isti.cnr.it:8080/cm}EventType" maxOccurs="unbounded" minOccurs="2"/>
 *         &lt;element name="time_interval" type="{http://urano.isti.cnr.it:8080/cm}TimeIntervalType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="operator" type="{http://urano.isti.cnr.it:8080/cm}EventNaryOperatorType" />
 *       &lt;attribute name="event_id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexNaryEventType", propOrder = {
    "event",
    "timeInterval"
})
public class ComplexNaryEventType {

    @XmlElement(required = true)
    protected List<EventType> event;
    @XmlElement(name = "time_interval")
    protected TimeIntervalType timeInterval;
    @XmlAttribute(name = "operator")
    protected EventNaryOperatorType operator;
    @XmlAttribute(name = "event_id")
    protected String eventId;

    /**
     * Gets the value of the event property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the event property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventType }
     * 
     * 
     */
    public List<EventType> getEvent() {
        if (event == null) {
            event = new ArrayList<EventType>();
        }
        return this.event;
    }

    /**
     * Recupera il valore della propriet� timeInterval.
     * 
     * @return
     *     possible object is
     *     {@link TimeIntervalType }
     *     
     */
    public TimeIntervalType getTimeInterval() {
        return timeInterval;
    }

    /**
     * Imposta il valore della propriet� timeInterval.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeIntervalType }
     *     
     */
    public void setTimeInterval(TimeIntervalType value) {
        this.timeInterval = value;
    }

    /**
     * Recupera il valore della propriet� operator.
     * 
     * @return
     *     possible object is
     *     {@link EventNaryOperatorType }
     *     
     */
    public EventNaryOperatorType getOperator() {
        return operator;
    }

    /**
     * Imposta il valore della propriet� operator.
     * 
     * @param value
     *     allowed object is
     *     {@link EventNaryOperatorType }
     *     
     */
    public void setOperator(EventNaryOperatorType value) {
        this.operator = value;
    }

    /**
     * Recupera il valore della propriet� eventId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * Imposta il valore della propriet� eventId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventId(String value) {
        this.eventId = value;
    }

}
