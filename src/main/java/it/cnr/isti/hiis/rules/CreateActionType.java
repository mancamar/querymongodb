//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.07.07 alle 11:40:34 AM CEST 
//


package it.cnr.isti.hiis.rules;

import eu.serenoa_fp7.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 This action creates a new model entity, optionally initializing its value
 *             
 * 
 * <p>Classe Java per CreateActionType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="CreateActionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="containingEntityReference" type="{http://www.serenoa-fp7.eu/}EntityReferenceType"/>
 *         &lt;group ref="{http://www.serenoa-fp7.eu/}TypeReferenceGroup"/>
 *         &lt;element name="value" type="{http://www.serenoa-fp7.eu/}CreateValuesType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="entity_id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateActionType", propOrder = {
    "containingEntityReference",
    "complexType",
    "simpleType",
    "values"
})
public class CreateActionType {

    @XmlElement(required = true)
    protected EntityReferenceType containingEntityReference;
    protected EntityReferenceType complexType;
    protected String simpleType;
    protected List<CreateValuesType> values;
    @XmlAttribute(name = "entity_id")
    protected String entityId;

    /**
     * Recupera il valore della propriet� containingEntityReference.
     * 
     * @return
     *     possible object is
     *     {@link EntityReferenceType }
     *     
     */
    public EntityReferenceType getContainingEntityReference() {
        return containingEntityReference;
    }

    /**
     * Imposta il valore della propriet� containingEntityReference.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityReferenceType }
     *     
     */
    public void setContainingEntityReference(EntityReferenceType value) {
        this.containingEntityReference = value;
    }

    /**
     * Recupera il valore della propriet� complexType.
     * 
     * @return
     *     possible object is
     *     {@link EntityReferenceType }
     *     
     */
    public EntityReferenceType getComplexType() {
        return complexType;
    }

    /**
     * Imposta il valore della propriet� complexType.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityReferenceType }
     *     
     */
    public void setComplexType(EntityReferenceType value) {
        this.complexType = value;
    }

    /**
     * Recupera il valore della propriet� simpleType.
     * 
     * @return
     *     possible object is
     *     {@link SimpleType }
     *     
     */
//    public SimpleType getSimpleType() {
//        return simpleType;
//    }

    /**
     * Imposta il valore della propriet� simpleType.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleType }
     *     
     */
//    public void setSimpleType(SimpleType value) {
//        this.simpleType = value;
//    }

    /**
     * Gets the value of the value property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the value property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateValuesType }
     * 
     * 
     */
    public List<CreateValuesType> getValues() {
        if (values == null) {
            values = new ArrayList<CreateValuesType>();
        }
        return this.values;
    }

    /**
     * Recupera il valore della propriet� entityId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * Imposta il valore della propriet� entityId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntityId(String value) {
        this.entityId = value;
    }

    public String getSimpleType() {
        return simpleType;
    }

    public void setSimpleType(String simpleType) {
        this.simpleType = simpleType;
    }
    
    
}
