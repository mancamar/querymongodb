//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per TimeIntervalType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="TimeIntervalType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startingTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="endingTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="specificDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="specificDays" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="7"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeIntervalType", propOrder = {
    "startingTime",
    "endingTime",
    "specificDate",
    "specificDays"
})
public class TimeIntervalType {

    @XmlElement(required = true)
    protected String startingTime;
    @XmlElement(required = true)
    protected String endingTime;
    protected String specificDate;
    @XmlElement(type = Integer.class)
    protected List<Integer> specificDays;

    /**
     * Recupera il valore della propriet� startingTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartingTime() {
        return startingTime;
    }

    /**
     * Imposta il valore della propriet� startingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartingTime(String value) {
        this.startingTime = value;
    }

    /**
     * Recupera il valore della propriet� endingTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndingTime() {
        return endingTime;
    }

    /**
     * Imposta il valore della propriet� endingTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndingTime(String value) {
        this.endingTime = value;
    }

    /**
     * Recupera il valore della propriet� specificDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificDate() {
        return specificDate;
    }

    /**
     * Imposta il valore della propriet� specificDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificDate(String value) {
        this.specificDate = value;
    }

    /**
     * Gets the value of the specificDays property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specificDays property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecificDays().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getSpecificDays() {
        if (specificDays == null) {
            specificDays = new ArrayList<Integer>();
        }
        return this.specificDays;
    }

}
