//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SimpleEventType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SimpleEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="entityReference" type="{http://urano.isti.cnr.it:8080/cm}EntityReferenceType"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="expression" type="{http://urano.isti.cnr.it:8080/cm}ExpressionType"/>
 *           &lt;element name="constant" type="{http://urano.isti.cnr.it:8080/cm}ConstantType"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *       &lt;attribute name="operator" type="{http://urano.isti.cnr.it:8080/cm}ConditionOperatorType" />
 *       &lt;attribute name="event_id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="event_name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="agent" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SimpleEventType", propOrder = {
    "entityReference",
    "expression",
    "constant"
})
public class SimpleEventType {

    @XmlElement(required = true)
    protected EntityReferenceType entityReference;
    protected ExpressionType expression;
    protected ConstantType constant;
    @XmlAttribute(name = "operator")
    protected ConditionOperatorType operator;
    @XmlAttribute(name = "event_id")
    protected String eventId;
    @XmlAttribute(name = "event_name")
    protected String eventName;
    @XmlAttribute(name = "agent")
    protected String agent;

    /**
     * Recupera il valore della propriet� entityReference.
     * 
     * @return
     *     possible object is
     *     {@link EntityReferenceType }
     *     
     */
    public EntityReferenceType getEntityReference() {
        return entityReference;
    }

    /**
     * Imposta il valore della propriet� entityReference.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityReferenceType }
     *     
     */
    public void setEntityReference(EntityReferenceType value) {
        this.entityReference = value;
    }

    /**
     * Recupera il valore della propriet� expression.
     * 
     * @return
     *     possible object is
     *     {@link ExpressionType }
     *     
     */
    public ExpressionType getExpression() {
        return expression;
    }

    /**
     * Imposta il valore della propriet� expression.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpressionType }
     *     
     */
    public void setExpression(ExpressionType value) {
        this.expression = value;
    }

    /**
     * Recupera il valore della propriet� constant.
     * 
     * @return
     *     possible object is
     *     {@link ConstantType }
     *     
     */
    public ConstantType getConstant() {
        return constant;
    }

    /**
     * Imposta il valore della propriet� constant.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstantType }
     *     
     */
    public void setConstant(ConstantType value) {
        this.constant = value;
    }

    /**
     * Recupera il valore della propriet� operator.
     * 
     * @return
     *     possible object is
     *     {@link ConditionOperatorType }
     *     
     */
    public ConditionOperatorType getOperator() {
        return operator;
    }

    /**
     * Imposta il valore della propriet� operator.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionOperatorType }
     *     
     */
    public void setOperator(ConditionOperatorType value) {
        this.operator = value;
    }

    /**
     * Recupera il valore della propriet� eventId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * Imposta il valore della propriet� eventId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventId(String value) {
        this.eventId = value;
    }

    /**
     * Recupera il valore della propriet� eventName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Imposta il valore della propriet� eventName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Recupera il valore della propriet� agent.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgent() {
        return agent;
    }

    /**
     * Imposta il valore della propriet� agent.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgent(String value) {
        this.agent = value;
    }

}
