//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ComplexUnaryEventType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ComplexUnaryEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="event" type="{http://urano.isti.cnr.it:8080/cm}EventType"/>
 *         &lt;element name="time_interval" type="{http://urano.isti.cnr.it:8080/cm}TimeIntervalType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="iteration_limit" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="operator" type="{http://urano.isti.cnr.it:8080/cm}EventUnaryOperatorType" />
 *       &lt;attribute name="event_id" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexUnaryEventType", propOrder = {
    "event",
    "timeInterval"
})
public class ComplexUnaryEventType {

    @XmlElement(required = true)
    protected EventType event;
    @XmlElement(name = "time_interval", required = true)
    protected TimeIntervalType timeInterval;
    @XmlAttribute(name = "iteration_limit")
    protected Integer iterationLimit;
    @XmlAttribute(name = "operator")
    protected EventUnaryOperatorType operator;
    @XmlAttribute(name = "event_id")
    protected String eventId;

    /**
     * Recupera il valore della propriet� event.
     * 
     * @return
     *     possible object is
     *     {@link EventType }
     *     
     */
    public EventType getEvent() {
        return event;
    }

    /**
     * Imposta il valore della propriet� event.
     * 
     * @param value
     *     allowed object is
     *     {@link EventType }
     *     
     */
    public void setEvent(EventType value) {
        this.event = value;
    }

    /**
     * Recupera il valore della propriet� timeInterval.
     * 
     * @return
     *     possible object is
     *     {@link TimeIntervalType }
     *     
     */
    public TimeIntervalType getTimeInterval() {
        return timeInterval;
    }

    /**
     * Imposta il valore della propriet� timeInterval.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeIntervalType }
     *     
     */
    public void setTimeInterval(TimeIntervalType value) {
        this.timeInterval = value;
    }

    /**
     * Recupera il valore della propriet� iterationLimit.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIterationLimit() {
        return iterationLimit;
    }

    /**
     * Imposta il valore della propriet� iterationLimit.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIterationLimit(Integer value) {
        this.iterationLimit = value;
    }

    /**
     * Recupera il valore della propriet� operator.
     * 
     * @return
     *     possible object is
     *     {@link EventUnaryOperatorType }
     *     
     */
    public EventUnaryOperatorType getOperator() {
        return operator;
    }

    /**
     * Imposta il valore della propriet� operator.
     * 
     * @param value
     *     allowed object is
     *     {@link EventUnaryOperatorType }
     *     
     */
    public void setOperator(EventUnaryOperatorType value) {
        this.operator = value;
    }

    /**
     * Recupera il valore della propriet� eventId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventId() {
        return eventId;
    }

    /**
     * Imposta il valore della propriet� eventId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventId(String value) {
        this.eventId = value;
    }

}
