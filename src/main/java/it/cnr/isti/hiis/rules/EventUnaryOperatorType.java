//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.14 alle 01:02:06 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EventUnaryOperatorType.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EventUnaryOperatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="not"/>
 *     &lt;enumeration value="iteration"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EventUnaryOperatorType")
@XmlEnum
public enum EventUnaryOperatorType {

    @XmlEnumValue("not")
    NOT("not"),
    @XmlEnumValue("iteration")
    ITERATION("iteration");
    private final String value;

    EventUnaryOperatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EventUnaryOperatorType fromValue(String v) {
        for (EventUnaryOperatorType c: EventUnaryOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
