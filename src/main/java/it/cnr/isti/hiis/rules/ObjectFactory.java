//
// Questo file è stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrà persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.19 alle 01:15:36 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.cnr.isti.hiis.rules package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Rule_QNAME = new QName("http://urano.isti.cnr.it:8080/cm", "rule");
    private final static QName _ActionsList_QNAME = new QName("http://urano.isti.cnr.it:8080/cm", "actionsList");
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.cnr.isti.hiis.rules
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RuleType }
     * 
     */
    public RuleType createRuleType() {
        return new RuleType();
    }

    /**
     * Create an instance of {@link DeleteActionType }
     * 
     */
    public DeleteActionType createDeleteActionType() {
        return new DeleteActionType();
    }

    /**
     * Create an instance of {@link InvokeFunctionParameterType }
     * 
     */
    public InvokeFunctionParameterType createInvokeFunctionParameterType() {
        return new InvokeFunctionParameterType();
    }

    /**
     * Create an instance of {@link EventType }
     * 
     */
    public EventType createEventType() {
        return new EventType();
    }

    /**
     * Create an instance of {@link WhileType }
     * 
     */
    public WhileType createWhileType() {
        return new WhileType();
    }

    /**
     * Create an instance of {@link RestExternalFunctionReferenceType }
     * 
     */
//    public RestExternalFunctionReferenceType createRestExternalFunctionReferenceType() {
//        return new RestExternalFunctionReferenceType();
//    }

    /**
     * Create an instance of {@link RelativeUpdateActionType }
     * 
     */
    public RelativeUpdateActionType createRelativeUpdateActionType() {
        return new RelativeUpdateActionType();
    }

    /**
     * Create an instance of {@link ForType }
     * 
     */
    public ForType createForType() {
        return new ForType();
    }

    /**
     * Create an instance of {@link SimpleType }
     * 
     */
    public SimpleType createSimpleType() {
        return new SimpleType();
    }

    /**
     * Create an instance of {@link NamedEntityReferenceType }
     * 
     */
    public NamedEntityReferenceType createNamedEntityReferenceType() {
        return new NamedEntityReferenceType();
    }

    /**
     * Create an instance of {@link EntityReferenceType }
     * 
     */
    public EntityReferenceType createEntityReferenceType() {
        return new EntityReferenceType();
    }

    /**
     * Create an instance of {@link ExternalModelType }
     * 
     */
    public ExternalModelType createExternalModelType() {
        return new ExternalModelType();
    }

    /**
     * Create an instance of {@link TimeIntervalType }
     * 
     */
    public TimeIntervalType createTimeIntervalType() {
        return new TimeIntervalType();
    }

    /**
     * Create an instance of {@link AbsoluteUpdateActionType }
     * 
     */
    public AbsoluteUpdateActionType createAbsoluteUpdateActionType() {
        return new AbsoluteUpdateActionType();
    }

    /**
     * Create an instance of {@link ExternalFunctionType }
     * 
     */
//    public ExternalFunctionType createExternalFunctionType() {
//        return new ExternalFunctionType();
//    }
//
//    /**
//     * Create an instance of {@link SoapExternalFunctionReference }
//     * 
//     */
//    public SoapExternalFunctionReference createSoapExternalFunctionReference() {
//        return new SoapExternalFunctionReference();
//    }

    /**
     * Create an instance of {@link IfThenElseType }
     * 
     */
    public IfThenElseType createIfThenElseType() {
        return new IfThenElseType();
    }

    /**
     * Create an instance of {@link SimpleEventType }
     * 
     */
    public SimpleEventType createSimpleEventType() {
        return new SimpleEventType();
    }

    /**
     * Create an instance of {@link ConstantType }
     * 
     */
    public ConstantType createConstantType() {
        return new ConstantType();
    }

    /**
     * Create an instance of {@link ExternalFunctionParameterType }
     * 
     */
//    public ExternalFunctionParameterType createExternalFunctionParameterType() {
//        return new ExternalFunctionParameterType();
//    }

    /**
     * Create an instance of {@link ExternalModelReferenceType }
     * 
     */
    public ExternalModelReferenceType createExternalModelReferenceType() {
        return new ExternalModelReferenceType();
    }

    /**
     * Create an instance of {@link ReadActionType }
     * 
     */
    public ReadActionType createReadActionType() {
        return new ReadActionType();
    }

    /**
     * Create an instance of {@link BlockType }
     * 
     */
    public BlockType createBlockType() {
        return new BlockType();
    }

    /**
     * Create an instance of {@link ComplexUnaryEventType }
     * 
     */
    public ComplexUnaryEventType createComplexUnaryEventType() {
        return new ComplexUnaryEventType();
    }

    /**
     * Create an instance of {@link CreateActionType }
     * 
     */
    public CreateActionType createCreateActionType() {
        return new CreateActionType();
    }

    /**
     * Create an instance of {@link InvokeFunctionType }
     * 
     */
    public InvokeFunctionType createInvokeFunctionType() {
        return new InvokeFunctionType();
    }

    /**
     * Create an instance of {@link IfThenType }
     * 
     */
    public IfThenType createIfThenType() {
        return new IfThenType();
    }

    /**
     * Create an instance of {@link ConditionType }
     * 
     */
    public ConditionType createConditionType() {
        return new ConditionType();
    }

    /**
     * Create an instance of {@link ComplexNaryEventType }
     * 
     */
    public ComplexNaryEventType createComplexNaryEventType() {
        return new ComplexNaryEventType();
    }

    /**
     * Create an instance of {@link ForEachType }
     * 
     */
    public ForEachType createForEachType() {
        return new ForEachType();
    }

    /**
     * Create an instance of {@link RulesType }
     * 
     */
    public RulesType createRulesType() {
        return new RulesType();
    }

    /**
     * Create an instance of {@link ExpressionType }
     * 
     */
    public ExpressionType createExpressionType() {
        return new ExpressionType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RuleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://urano.isti.cnr.it:8080/cm", name = "rule")
    public JAXBElement<RuleType> createRule(RuleType value) {
        return new JAXBElement<RuleType>(_Rule_QNAME, RuleType.class, null, value);
    }

    @XmlElementDecl(namespace = "http://urano.isti.cnr.it:8080/cm", name = "actionsList")
    public JAXBElement<ActionsType> createActionsList(ActionsType value) {
        return new JAXBElement<ActionsType>(_ActionsList_QNAME, ActionsType.class, null, value);
    }
}
