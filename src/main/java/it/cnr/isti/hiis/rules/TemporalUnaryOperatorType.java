//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2018.12.19 alle 01:15:36 PM CET 
//


package it.cnr.isti.hiis.rules;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per TemporalUnaryOperatorType.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TemporalUnaryOperatorType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="zero_more_occur"/>
 *     &lt;enumeration value="one_more_occur"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TemporalUnaryOperatorType")
@XmlEnum
public enum TemporalUnaryOperatorType {

    @XmlEnumValue("zero_more_occur")
    ZERO_MORE_OCCUR("zero_more_occur"),
    @XmlEnumValue("one_more_occur")
    ONE_MORE_OCCUR("one_more_occur");
    private final String value;

    TemporalUnaryOperatorType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TemporalUnaryOperatorType fromValue(String v) {
        for (TemporalUnaryOperatorType c: TemporalUnaryOperatorType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
