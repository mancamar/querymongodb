//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.07.21 at 04:03:59 PM CEST 
//


package eu.serenoa_fp7;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for xsdSimpleTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="xsdSimpleTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="anyURI"/>
 *     &lt;enumeration value="base64Binary"/>
 *     &lt;enumeration value="boolean"/>
 *     &lt;enumeration value="byte"/>
 *     &lt;enumeration value="date"/>
 *     &lt;enumeration value="dateTime"/>
 *     &lt;enumeration value="decimal"/>
 *     &lt;enumeration value="duration"/>
 *     &lt;enumeration value="float"/>
 *     &lt;enumeration value="gDay"/>
 *     &lt;enumeration value="gMonthDay"/>
 *     &lt;enumeration value="gYear"/>
 *     &lt;enumeration value="gYearMonth"/>
 *     &lt;enumeration value="hexBinary"/>
 *     &lt;enumeration value="int"/>
 *     &lt;enumeration value="integer"/>
 *     &lt;enumeration value="language"/>
 *     &lt;enumeration value="long"/>
 *     &lt;enumeration value="negativeInteger"/>
 *     &lt;enumeration value="nonPositiveInteger"/>
 *     &lt;enumeration value="normalizedString"/>
 *     &lt;enumeration value="positiveInteger"/>
 *     &lt;enumeration value="short"/>
 *     &lt;enumeration value="string"/>
 *     &lt;enumeration value="time"/>
 *     &lt;enumeration value="token"/>
 *     &lt;enumeration value="unsignedInt"/>
 *     &lt;enumeration value="unsignedLong"/>
 *     &lt;enumeration value="unsignedShort"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "xsdSimpleTypeEnum")
@XmlEnum
public enum XsdSimpleTypeEnum {

    @XmlEnumValue("anyURI")
    ANY_URI("anyURI"),
    @XmlEnumValue("base64Binary")
    BASE_64_BINARY("base64Binary"),
    @XmlEnumValue("boolean")
    BOOLEAN("boolean"),
    @XmlEnumValue("byte")
    BYTE("byte"),
    @XmlEnumValue("date")
    DATE("date"),
    @XmlEnumValue("dateTime")
    DATE_TIME("dateTime"),
    @XmlEnumValue("decimal")
    DECIMAL("decimal"),
    @XmlEnumValue("duration")
    DURATION("duration"),
    @XmlEnumValue("float")
    FLOAT("float"),
    @XmlEnumValue("gDay")
    G_DAY("gDay"),
    @XmlEnumValue("gMonthDay")
    G_MONTH_DAY("gMonthDay"),
    @XmlEnumValue("gYear")
    G_YEAR("gYear"),
    @XmlEnumValue("gYearMonth")
    G_YEAR_MONTH("gYearMonth"),
    @XmlEnumValue("hexBinary")
    HEX_BINARY("hexBinary"),
    @XmlEnumValue("int")
    INT("int"),
    @XmlEnumValue("integer")
    INTEGER("integer"),
    @XmlEnumValue("language")
    LANGUAGE("language"),
    @XmlEnumValue("long")
    LONG("long"),
    @XmlEnumValue("negativeInteger")
    NEGATIVE_INTEGER("negativeInteger"),
    @XmlEnumValue("nonPositiveInteger")
    NON_POSITIVE_INTEGER("nonPositiveInteger"),
    @XmlEnumValue("normalizedString")
    NORMALIZED_STRING("normalizedString"),
    @XmlEnumValue("positiveInteger")
    POSITIVE_INTEGER("positiveInteger"),
    @XmlEnumValue("short")
    SHORT("short"),
    @XmlEnumValue("string")
    STRING("string"),
    @XmlEnumValue("time")
    TIME("time"),
    @XmlEnumValue("token")
    TOKEN("token"),
    @XmlEnumValue("unsignedInt")
    UNSIGNED_INT("unsignedInt"),
    @XmlEnumValue("unsignedLong")
    UNSIGNED_LONG("unsignedLong"),
    @XmlEnumValue("unsignedShort")
    UNSIGNED_SHORT("unsignedShort");
    private final String value;

    XsdSimpleTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static XsdSimpleTypeEnum fromValue(String v) {
        for (XsdSimpleTypeEnum c: XsdSimpleTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
