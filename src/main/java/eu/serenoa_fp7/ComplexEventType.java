//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.02.01 at 01:08:05 PM CET 
//


package eu.serenoa_fp7;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *                 An event could be specified through Boolean expression combining multiple events
 *             
 * 
 * <p>Java class for ComplexEventType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplexEventType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded" minOccurs="2">
 *         &lt;element name="event" type="{http://www.serenoa-fp7.eu/}EventType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="operator" type="{http://www.serenoa-fp7.eu/}BooleanOperatorType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplexEventType", propOrder = {
    "event"
})
public class ComplexEventType {

    @XmlElement(required = true)
    protected List<EventType> event;
    @XmlAttribute(name = "operator")
    protected BooleanOperatorType operator;
    @XmlAttribute(name = "max_interval", required = true)
    protected int max_interval;

    /**
     * Gets the value of the event property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the event property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventType }
     * 
     * 
     */
    public List<EventType> getEvent() {
        if (event == null) {
            event = new ArrayList<EventType>();
        }
        return this.event;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link BooleanOperatorType }
     *     
     */
    public BooleanOperatorType getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link BooleanOperatorType }
     *     
     */
    public void setOperator(BooleanOperatorType value) {
        this.operator = value;
    }

    public int getMax_interval() {
        return max_interval;
    }

    public void setMax_interval(int max_interval) {
        this.max_interval = max_interval;
    }

    
}
