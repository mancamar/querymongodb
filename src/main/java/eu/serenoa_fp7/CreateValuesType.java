//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.07.07 alle 11:40:34 AM CEST 
//


package eu.serenoa_fp7;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per CreateValuesType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="CreateValuesType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="entityReference" type="{http://www.serenoa-fp7.eu/}EntityReferenceType" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="constantValue" type="{http://www.serenoa-fp7.eu/}ConstantType" minOccurs="0"/>
 *           &lt;element name="referenceValue" type="{http://www.serenoa-fp7.eu/}EntityReferenceType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateValuesType", propOrder = {
    "entityReference",
    "constantValue",
    "referenceValue"
})
public class CreateValuesType {

    protected EntityReferenceType entityReference;
    protected ConstantType constantValue;
    protected EntityReferenceType referenceValue;

    /**
     * Recupera il valore della propriet� entityReference.
     * 
     * @return
     *     possible object is
     *     {@link EntityReferenceType }
     *     
     */
    public EntityReferenceType getEntityReference() {
        return entityReference;
    }

    /**
     * Imposta il valore della propriet� entityReference.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityReferenceType }
     *     
     */
    public void setEntityReference(EntityReferenceType value) {
        this.entityReference = value;
    }

    /**
     * Recupera il valore della propriet� constantValue.
     * 
     * @return
     *     possible object is
     *     {@link ConstantType }
     *     
     */
    public ConstantType getConstantValue() {
        return constantValue;
    }

    /**
     * Imposta il valore della propriet� constantValue.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstantType }
     *     
     */
    public void setConstantValue(ConstantType value) {
        this.constantValue = value;
    }

    /**
     * Recupera il valore della propriet� referenceValue.
     * 
     * @return
     *     possible object is
     *     {@link EntityReferenceType }
     *     
     */
    public EntityReferenceType getReferenceValue() {
        return referenceValue;
    }

    /**
     * Imposta il valore della propriet� referenceValue.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityReferenceType }
     *     
     */
    public void setReferenceValue(EntityReferenceType value) {
        this.referenceValue = value;
    }

}
